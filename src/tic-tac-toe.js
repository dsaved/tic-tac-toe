const Configuration = require('./configs');
const _Readline = require('readline')
const possibleOutComes = require('./possible-outcomes')

'use strict';

const Reset = "\x1b[0m"
const Dim = "\x1b[2m"

const FgRed = "\x1b[31m"
const FgGreen = "\x1b[32m"
const FgYellow = "\x1b[33m"
const FgMagenta = "\x1b[35m"
const FgCyan = "\x1b[36m"
const FgWhite = "\x1b[37m"

class TicTacToe {
    constructor() {
        // initiate variables
        this.ticTacToeLayout = '';
        this.currentPlayer = null; // AI player = null (X), player 1 = true (O), player 2 = false (W)
        this.gameEnded = false;
        this.registeredMoves = [];
        this.randomStartArr = [null, true, false]

        this.conf = Configuration.getConfig();

        // this valriable will hold the game play of each user
        this.gamePlay = [
            { player: "AI", play: [], char: [] },
            { player: "1", play: [], char: [] },
            { player: "2", play: [], char: [] },
        ];

        //Playes variables
        this.playerAI = "AI"
        this.player1 = "1"
        this.player2 = "2"

        //Get game configuration from file
        this.playgorundSize = Number(this.conf.board_size)
        if (this.playgorundSize > 10 || this.playgorundSize < 3) {
            throw new Error(`Playground size error: Expected number 3 to 10, got ${this.playgorundSize}`);
        }
        this.symbol1 = this.conf.symbol1
        this.symbol2 = this.conf.symbol2
        this.symbol3 = this.conf.symbol3

        this.maxLength = Math.pow(this.playgorundSize, 2)
        this.indexSize = this.maxLength - 1

        /* 
        generate empty array for the number of board space
        and insert place holders to identify play positions
         */
        this.ticTacToe = Array.apply(null, { length: this.maxLength }).map(Function.call, (nul) => {
            const num = nul + 1
            let outNum = `${num}`;
            if (num < 10) { outNum = `0${num}`; }
            return outNum
        });

        this.readLine = _Readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })

        console.log(`\n`)
        console.log(`${FgMagenta} TicTacToe Game 2.0`)
        console.log(`-------------------- ${Reset} \n`)

    }

    // start game
    startGame() {
        this.displayLayout();

        const random = Math.floor(Math.random() * this.randomStartArr.length);
        this.currentPlayer = this.randomStartArr[random]
        this.playOn()

        // listen to inputs
        this.readLine.on("line", (input) => {
            this.readMove(parseInt(input))
        })
    }


    // Draw data to console
    drawLayout() {
        const multiDimArray = this.splitToMultyArray()
        this.ticTacToeLayout = `${FgCyan}` + '+++'.repeat(this.charLent(this.playgorundSize)) + `${Reset}\n`
        for (let x = 0; x < multiDimArray.length; x++) {
            for (let y = 0; y < multiDimArray[x].length; y++) {
                this.ticTacToeLayout += y === 0 ? `${FgWhite}${this.displayItem(multiDimArray[x][y])}${Reset}` : ` ${FgCyan}|${Reset} ${this.displayItem(multiDimArray[x][y])}`
            }
            this.ticTacToeLayout += `\n`
            if (x !== multiDimArray.length - 1) {
                this.ticTacToeLayout += `${FgCyan}` + '---'.repeat(this.charLent(this.playgorundSize)) + `${Reset}`
                this.ticTacToeLayout += `\n`
            }
        }
        this.ticTacToeLayout += `${FgCyan}` + '+++'.repeat(this.charLent(this.playgorundSize)) + `${Reset}\n\n`
    }

    /* 
    Generate game board by spliting main
    array into chunks of playgroundSize 
    */
    splitToMultyArray() {
        let arrayOfArrays = [],
            tempArray = JSON.parse(JSON.stringify(this.ticTacToe))
        while (tempArray.length > 0) {
            arrayOfArrays.push(tempArray.splice(0, this.playgorundSize))
        }
        return arrayOfArrays
    }

    //helper function.
    charLent(num) {
        let characters = `00`.repeat(num).trim()
        return characters.length
    }

    // AI play start
    async AI() {
        // delay for 2secs
        await this.delay(2);

        // get available empty board slots
        const slots = this.getAvailableSlots()
        if (slots.length <= 0) {
            // end the game if no available slots
            this.endGame()
        } else {
            // make a move on a random slot
            const random = Math.floor(Math.random() * slots.length);
            this.readMove(parseInt(slots[random]))
        }
    }

    // end the game
    endGame(terminate = true) {
        this.readLine.close();
        this.gameEnded = true;
        console.log(`${FgRed}Game ended!! ${Reset}`)
        if (terminate) { process.exit() }
        return false;
    }

    // continue playing
    playOn() {
        if (!this.gameEnded) {
            // switch player
            switch (this.currentPlayer) {
                case true:
                    this.currentPlayer = false
                    break;
                case false:
                    this.currentPlayer = null
                    this.AI()
                    break;
                case null:
                    this.currentPlayer = true
                    break;
                default:
                    break;
            }
            console.log(`Player ${this.displayPlayer(this.currentPlayer)} `)
        }
    }

    processGame() {
        // get the current user play and check if player has won the game
        const foundPlayer = this.gamePlay.find((play) => play.player === this.displayPlayer(this.currentPlayer))
        const won = possibleOutComes.isWin(foundPlayer)
        if (won) {
            console.log(`${FgGreen} player ${foundPlayer.player} wins the game ${Reset}`)
                // end the game
            this.endGame()
        } else {
            // check for available move
            const slots = this.getAvailableSlots()
            if (slots.length <= 0) {
                console.log(`${FgYellow}No winner for this game ${Reset}`)
                this.endGame()
            } else {
                // continue playing
                this.playOn();
            }
        }
    }

    // helper function: display played color or place holder color
    displayItem(item) {
        return isNaN(item) ? `${FgYellow} ${item} ${Reset}` : `${Dim} ${item}${Reset}`
    }

    // delay by seconds 
    delay(sec) {
        return new Promise(res => setTimeout(res, sec * 1000));
    }

    // check the board for available play slot
    getAvailableSlots() {
        let slots = []
        for (let i = 0; i < this.ticTacToe.length; i++) {
            if (isNaN(`${this.ticTacToe[i]}`) === false) {
                slots.push(i + 1)
            }
        }
        return slots
    }

    // get the name of player from already played data
    getPlayerFromChar(char) {
        const found = this.registeredMoves.find((object) => object.char === char)
        return found ? found.player : ""
    }

    // display current player name
    displayPlayer(plyr) {
        if (plyr === null) {
            return this.playerAI
        }
        if (plyr === true) {
            return this.player1
        }
        return this.player2
    }

    // get character symbol
    getCharacter(plyr) {
        if (plyr === null) {
            return this.symbol1
        }
        if (plyr === true) {
            return this.symbol2
        }
        return this.symbol3
    }

    displayLayout() {
        this.drawLayout()
        console.log(this.ticTacToeLayout);
    }

    readMove(position) {
        var self = this

        if (isNaN(position)) {
            // position occupied
            self.moveError(`${FgRed} Invalid input!!!${Reset} `);
            return
        }

        // check if poosition is eligible
        if ((position > this.maxLength) || position < 1) {
            // wrong position
            self.moveError(`${FgRed}Wrong position!!!${Reset} `);
            //check if player is AI then plaay again
            if (this.currentPlayer === null) {
                this.AI();
            }
            return
        }

        // check if position is occupied
        if (isNaN(`${self.ticTacToe[(position - 1)]}`)) {
            // position occupied
            self.moveError(`${FgRed}Position Occupied!!!${Reset} `);
            //check if player is AI then plaay again
            if (this.currentPlayer === null) {
                this.AI();
            }
        } else {
            // register move
            self.ticTacToe[(position - 1)] = self.getCharacter(self.currentPlayer);
            // record move
            self.recordMove((position - 1));
            self.displayLayout();
            self.processGame();
        }
    }

    // notify wrong moves
    moveError(message) {
        console.log(`${message ? message:''} Player ${this.displayPlayer(this.currentPlayer)}, Your move!: `)
    }

    // record players move to use when checking for a winner
    recordMove(position) {
        this.registeredMoves.push({
            position: position,
            char: this.getCharacter(this.currentPlayer),
            player: this.displayPlayer(this.currentPlayer)
        });

        this.gamePlay.forEach((play, index) => {
            if (play.player === this.displayPlayer(this.currentPlayer)) {
                this.gamePlay[index].play.push(position)
                this.gamePlay[index].char.push(this.getCharacter(this.currentPlayer))
            }
        })
    }

}

module.exports = new TicTacToe()