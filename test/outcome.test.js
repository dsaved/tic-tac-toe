const expect = require('chai').expect;
const outCome = require('../src/possible-outcomes')

// Default test board size setting 3
describe('possible-outcomes.js tests', () => {

    describe('outcomes.isWin() Test', () => {
        it('should equal false', () => {
            const won = outCome.isWin({ player: 'Dsaved', play: [0, 2, 4], char: [] })
            expect(won).to.equal(false);
        });

        it('should equal true', () => {
            const won = outCome.isWin({ player: 'Dsaved', play: [3, 4, 5], char: [] })
            expect(won).to.equal(true);
        });
    });

    describe('outcomes.equals() Test', () => {
        it('should equal false', () => {
            const checkWin = outCome.equals([1, 4, 5, 2, 3], [2, 4, 67, 10, 3, 13, 4])
            expect(checkWin).to.equal(false);
        });

        it('should equal true', () => {
            const checkWin = outCome.equals([2, 6, 10, 14], [14, 6, 2, 10, 3, 13, 4])
            expect(checkWin).to.equal(true);
        });
    });
});