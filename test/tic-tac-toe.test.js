const expect = require('chai').expect;
const ticTacToe = require('../src/tic-tac-toe')

// Default test board size setting 3
describe('tic-tac-toe.js tests', () => {

    describe('ticTacToe.getAvailableSlots() Test', () => {
        it('should equal [1, 2, 3, 4, 5, 6, 7, 8, 9]', () => {
            const slots = ticTacToe.getAvailableSlots()

            // this will fail if the game board size is changed
            expect(slots).to.eql([1, 2, 3, 4, 5, 6, 7, 8, 9]);
        });
    });

    describe('ticTacToe.splitToMultyArray() Test', () => {
        it('should equal [["01", "02", "03"], ["04", "05", "06"], ["07", "08", "09"]]', () => {
            const board = ticTacToe.splitToMultyArray()
                // this will fail if the game board size is changed
            expect(board).to.eql([
                ["01", "02", "03"],
                ["04", "05", "06"],
                ["07", "08", "09"]
            ]);
        });
    });

    describe('ticTacToe.charLent() Test', () => {
        it('should equal 6', () => {
            const charLent = ticTacToe.charLent(3)
            expect(charLent).to.equal(6);
        });
    });

    describe('ticTacToe.displayItem() Test', () => {
        it('should equal \u001b[2m 3\u001b[0m', () => {
            const displayItem = ticTacToe.displayItem(3)
            expect(displayItem).to.equal("\u001b[2m 3\u001b[0m");
        });
        it('should equal \u001b[33m X \u001b[0m', () => {
            const displayItem = ticTacToe.displayItem("X")
            expect(displayItem).to.equal("\u001b[33m X \u001b[0m");
        });
    });

    describe('ticTacToe.displayPlayer() Test', () => {
        it('should equal AI', () => {
            const displayItem = ticTacToe.displayPlayer(null)
            expect(displayItem).to.equal("AI");
        });
        it('should equal 1', () => {
            const displayItem = ticTacToe.displayPlayer(true)
            expect(displayItem).to.equal("1");
        });
        it('should equal 2', () => {
            const displayItem = ticTacToe.displayPlayer(false)
            expect(displayItem).to.equal("2");
        });
    });

    describe('ticTacToe.getCharacter() Test', () => {
        it('should equal X', () => {
            const getCharacter = ticTacToe.getCharacter(null)
            expect(getCharacter).to.equal("X");
        });
        it('should equal O', () => {
            const getCharacter = ticTacToe.getCharacter(true)
            expect(getCharacter).to.equal("O");
        });
        it('should equal W', () => {
            const getCharacter = ticTacToe.getCharacter(false)
            expect(getCharacter).to.equal("W");
        });
    });

    describe('ticTacToe void method Test', () => {
        it('moveError should Have Been Called', () => {
            // Check internal function
            expect(ticTacToe.moveError("error message"));
        });

        it('recordMove should Have Been Called', () => {
            // Check internal function
            expect(ticTacToe.recordMove(2));
        });

        it('processGame should Have Been Called', () => {
            // Check internal function
            expect(ticTacToe.processGame());
        });

        it('readMove should Have Been Called', () => {
            // Check internal function
            expect(ticTacToe.readMove(2));
        });

        it('displayLayout should Have Been Called', () => {
            // Check internal function
            expect(ticTacToe.displayLayout());
        });

    });

    describe('ticTacToe.getPlayerFromChar() Test', () => {

        it('should equal "AI"', () => {
            const getPlayerFromChar = ticTacToe.getPlayerFromChar("X")
            expect(getPlayerFromChar).to.equal("AI");
        });

        it('should equal "1"', () => {
            const getPlayerFromChar = ticTacToe.getPlayerFromChar("O")
            expect(getPlayerFromChar).to.equal("1");
        });

        it('should equal ""', () => {
            /* get the name of player from already played data
            because player 2 has no record of played move the test will return "" 
            */
            const getPlayerFromChar = ticTacToe.getPlayerFromChar("W")
            expect(getPlayerFromChar).to.equal("");
        });
    });

    describe('ticTacToe endGame method Test', () => {
        it('endGame should Have Been Called', () => {
            // Check internal function
            expect(ticTacToe.endGame(false));
        });
    });
});