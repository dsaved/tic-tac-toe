# Tic tac toe

A simple terminal TicTacToe game made for 3 players where one of the players is an AI. the game has a configurable play board and player character. The play board can be configure to any number of size e.g (3x3, 4x4, ...10x10). [Editing the configuration file](#editing-configuration-file)!

## Getting started

You need to have nodejs installed in your system. 

install nodejs from [Here](https://nodejs.org/en/download/)

* Clone the reppsitory or simply download and extract to you system folder.

- Switch to the projects directory and install test libraries if you would like to test the project before runing it.

```
npm install mocha --save-dev
npm install chai --save-dev
```
##### Editing configuration file
In order to edit the configuration file, open the config.json file
```
{
    "board_size": 3,
    "symbol1": "X",
    "symbol2": "O",
    "symbol3": "W"
}
```
* the *"board_size"* determines the size of the play bord
* the *"symbol1"* represents the AI character
* the *"symbol2"* represents the player 1 character
* the *"symbol3"* represents the player 2 character

##### to run test 
In order for all the test to pass, you need to leave the default configuration in the config.json file
```
npm run test
```

##### to run the game 
```
node index.js
```
